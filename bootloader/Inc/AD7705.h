/**
  ******************************************************************************
  * @file    crc.h 
  * @author  AKabanov
  * @brief   Header for crc.c module
  ******************************************************************************
  ******************************************************************************
  */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __AD7705_H
#define __AD7705_H

/* Includes ------------------------------------------------------------------*/
#include "stm32f2xx_hal.h"
#include "intrinsics.h"

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
/**
  * @brief  initialization ADC
  * @param               
  * @retval         
  */
    
void AD7705init(void);
/**
  * @brief  deinitialization ADC
  * @param               
  * @retval         
  */
void AD7705deinit(void);

/**
  * @brief  read an adc  value from the channel 1
  * @param         
  * @retval data from adc 
  */
uint16_t AD7705ReadCh1(void);

/**
  * @brief  read an adc  value from the channel 2
  * @param         
  * @retval  data from adc
  */
int16_t AD7705ReadCh2(void);

#endif /* __AD7705_H */
