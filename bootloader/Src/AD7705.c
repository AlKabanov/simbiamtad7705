/**
  ******************************************************************************
  * @file    AD7705.c  
  * @author  Kabanov A.
  * @brief   
  *                      Driver for analog device AD7705                 
  *                                                                    
  *     adc_init()                    Call after power up                   
  *                                                                    
  *     read_adc_value(channel)Read adc value from the specified channel   
  *                                                                   
  *     adc_disable()    Disables the adc conversion	
  * Assuming a 2.4576 crystal ocsillator is used between MCLK IN and MCLK OUT
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "AD7705.h"
#include "gpio.h"
    

extern void _Error_Handler(char *, int);

/** @addtogroup AD7705
  * @{
  */ 
  
/* Private typedef -----------------------------------------------------------*/

/* Private define ------------------------------------------------------------*/
//Operation modes
#define ADC_NORMAL 0x00
#define ADC_SELF 0x40
#define ADC_ZERO_SCALE 0x80
#define ADC_FULL_SCALE 0xc0

//Gain settings
#define ADC_GAIN_1 0x00
#define ADC_GAIN_2 0x08
#define ADC_GAIN_4 0x10
#define ADC_GAIN_8 0x18
#define ADC_GAIN_16 0x20
#define ADC_GAIN_32 0x28
#define ADC_GAIN_64 0x30
#define ADC_GAIN_128 0x38

//Polar operations
#define ADC_BIPOLAR 0x00
#define ADC_UNIPOLAR 0x04

//update rates
#define ADC_50 0x04
#define ADC_60 0x05
#define ADC_250 0x06
#define ADC_500 0x07

#define ADC_READ  0x08  //read bit
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

void write_adc_byte(uint8_t data)
{
   //gpioADCSOff();
   for(int i = 0; i < 8; i++) {
      gpioCLKOff();
      uint8_t sendDeb = data & (0x80 >> i);
      gpioMOSI(sendDeb);       //MSB first
      gpioCLKOn();
   }
   //gpioADCSOn();
}
uint8_t read_adc_byte(void)
{
  uint8_t value = 0;
  //gpioADCSOff();
  for(int i = 0; i < 8; i++) 
  {
      gpioCLKOff();
      __no_operation();
      if(gpioReadMISO()) value |= 0x80U >> i;  //MSB first         
      gpioCLKOn();
  }
  //gpioADCSOn();
  return value;
}

uint16_t read_adc_word(void)
{
  uint16_t value = 0;
  //gpioADCSOff();
  for(int i = 0; i < 16 ; i++) 
  {
      gpioCLKOff();
      __no_operation();
      if(gpioReadMISO()) value |= 0x8000U >> i; //MSB first   
      gpioCLKOn();
  }
  //gpioADCSOn();
  return value;
  
}

//setup the device paramaters(mode, gainsetting, polar operation and output rate)
void setup_adc_device(uint8_t calmode, uint8_t gainsetting, uint8_t operation, uint8_t rate)
{
    write_adc_byte( 0x20 );//Communications Register set to write of clock register
    write_adc_byte( rate );//Clock Register info here
    write_adc_byte( 0x10 );//Communications Register set to write of setup register
    write_adc_byte( calmode|gainsetting|operation);//Setup Register info here
}

/* Public functions ----------------------------------------------------------*/
uint8_t debugADCRead = 0;
void AD7705init(void)
{
    
    gpioADCSOff();                       //Set low to AD7715 chip select low pin
    gpioADResetOff();    
    gpioCLKOn();   
    HAL_Delay(1); 
    gpioADResetOn();            //Set high to AD7715 reset low pin
    setup_adc_device(ADC_SELF,ADC_GAIN_128,ADC_BIPOLAR,ADC_50);
    HAL_Delay(3000); 

}
    
//disable the a/d conversion
void AD7705deinit(void)
{
 	write_adc_byte( 0x20 );//Communications Register set to write of clock register
	write_adc_byte( 0x10 );//master clock disable
        gpioADCSOn();                       //Set high to AD7715 chip select low pin
}

/**
  * @brief  read an adc  value from the channel 1
  * @param         
  * @retval data from adc 
  */
uint16_t AD7705ReadCh1()
{
   uint16_t value;
   write_adc_byte(0x08);//communications register set to read of communication register
   debugADCRead = read_adc_byte();
   write_adc_byte(0x18);//communications register set to read of setup register
   debugADCRead = read_adc_byte();
   while ( !gpioReadDRDY());
   write_adc_byte(0x38);//communications register set to read of data register of channel 1
   value = read_adc_word();
   while ( gpioReadDRDY());
   return value;
}
/**
  * @brief  read an adc  value from the channel 2
  * @param         
  * @retval  data from adc
  */
int16_t AD7705ReadCh2()
{
  
   int16_t value;
   while ( !gpioReadDRDY());
   write_adc_byte(0x39);//communications register set to read of data register of channel 2
   value = read_adc_word();
   while ( gpioReadDRDY());
   return value;
}
/**
  * @brief  
  * @param  
  *         
  * @retval 
  */

  
/**
  * @}
  */ 

/*****************************END OF FILE****/
