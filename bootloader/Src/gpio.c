/**
  ******************************************************************************
  * @file           : gpio.c
  * @brief          : all routines connected with gpio
  ******************************************************************************
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "gpio.h"

/** @addtogroup GPIO
  * @{
  */ 

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define MISO_PORT GPIOC
#define MISO_PIN GPIO_PIN_12
#define MOSI_PORT GPIOC
#define MOSI_PIN GPIO_PIN_11
#define CLK_PORT GPIOC
#define CLK_PIN  GPIO_PIN_10
#define DRDY_PORT GPIOA
#define DRDY_PIN GPIO_PIN_15
#define AD_RESET_PORT GPIOD
#define AD_RESET_PIN GPIO_PIN_2
#define AD_CS_PORT GPIOB
#define AD_CS_PIN  GPIO_PIN_3

#define SEN_PWR_PORT GPIOA
#define SEN_PWR_PIN GPIO_PIN_2


/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
static GPIO_InitTypeDef  GPIO_InitStruct;

/* Private function prototypes -----------------------------------------------*/

/* Public functions ----------------------------------------------------------*/
/**
  * @brief  initializing all GPIOs
  * @param  None
  * @retval None
  */
void gpioInit(void)
{
  //PORT A
  /* -1- Enable GPIOA Clock (to be able to program the configuration registers) */
  __HAL_RCC_GPIOA_CLK_ENABLE();
  
  GPIO_InitStruct.Pin = (GPIO_PIN_8|GPIO_PIN_0|SEN_PWR_PIN);
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FAST;
  
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
  
  /* -2- Configure PA.15 IOs in input push-pull mode  DRDY*/
  GPIO_InitStruct.Pin = (DRDY_PIN);
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FAST;
  
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
  //PORT B
  /* -1- Enable GPIOB Clock (to be able to program the configuration registers) */
  __HAL_RCC_GPIOB_CLK_ENABLE();
  
  GPIO_InitStruct.Pin = (AD_CS_PIN);
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FAST;
  
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
  
  //PORT C
  /* -1- Enable GPIOC Clock (to be able to program the configuration registers) */
  __HAL_RCC_GPIOC_CLK_ENABLE();  
    
  GPIO_InitStruct.Pin = (GPIO_PIN_3|GPIO_PIN_0|CLK_PIN|MOSI_PIN);
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FAST;
  
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
 
  /* -2- Configure PC.11, IOs in input push-pull mode MISO*/ 
  GPIO_InitStruct.Pin = (MISO_PIN);
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FAST;
  
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
  
  //PORT D
  /* -1- Enable GPIOD Clock (to be able to program the configuration registers) */
  __HAL_RCC_GPIOD_CLK_ENABLE();
  
  GPIO_InitStruct.Pin = (AD_RESET_PIN);
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FAST;
  
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);  
}
/**
  * @brief  deinitializing all GPIOs
  * @param  None
  * @retval None
  */
void gpioDeInit(void)
{
  HAL_GPIO_DeInit(GPIOA, GPIO_PIN_0|GPIO_PIN_8|DRDY_PIN|SEN_PWR_PIN);
  HAL_GPIO_DeInit(GPIOB, AD_CS_PIN);
  HAL_GPIO_DeInit(GPIOC, GPIO_PIN_3|GPIO_PIN_0|CLK_PIN|MOSI_PIN|MISO_PIN);
  HAL_GPIO_DeInit(GPIOD, GPIO_PIN_2);
}
/**
  * @brief  set GPIOC 3, close U2
  * @param  None
  * @retval None
  */
void gpioTxEn(void)
{
  HAL_GPIO_WritePin(GPIOC, GPIO_PIN_3, GPIO_PIN_SET);
}

/**
  * @brief  clear GPIOC 3
  * @param  None
  * @retval None
  */
void gpioRxEn(void)
{
  HAL_GPIO_WritePin(GPIOC, GPIO_PIN_3, GPIO_PIN_RESET);
}

/**
  * @brief  clear GPIOA 0
  * @param  None
  * @retval None
  */
void gpioLEDOff(void)
{
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_0, GPIO_PIN_RESET);
}

/**
  * @brief  set GPIOA 0
  * @param  None
  * @retval None
  */
void gpioLEDOn(void)
{
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_0, GPIO_PIN_SET);
}

void gpioPWROn(void)
{
  HAL_GPIO_WritePin(GPIOC, GPIO_PIN_0, GPIO_PIN_SET);
}
void gpioPWROff(void)
{
  HAL_GPIO_WritePin(GPIOC, GPIO_PIN_0, GPIO_PIN_RESET);
}
void gpioRedLEDOff(void)
{
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8, GPIO_PIN_RESET);
}

void gpioRedLEDOn(void)
{
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8, GPIO_PIN_SET);
}

void gpioRedLEDToggle(void)
{
  HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_8);
}
/*******************************************************************************
*               Sensor power pin functions
*******************************************************************************/
void gpioSenPWROn(void)
{
  HAL_GPIO_WritePin(SEN_PWR_PORT, SEN_PWR_PIN, GPIO_PIN_SET);
}
void gpioSenPWROff(void)
{
  HAL_GPIO_WritePin(SEN_PWR_PORT, SEN_PWR_PIN, GPIO_PIN_RESET);
}


/*******************************************************************************
*               AD7705 control pin functions
*******************************************************************************/
uint8_t gpioReadMISO(void)
{
  return HAL_GPIO_ReadPin(MISO_PORT, MISO_PIN);
}

uint8_t gpioReadDRDY(void)
{
  return HAL_GPIO_ReadPin(DRDY_PORT, DRDY_PIN);
}

void gpioADResetOn(void)
{
  HAL_GPIO_WritePin(AD_RESET_PORT, AD_RESET_PIN, GPIO_PIN_SET);
}

void gpioADResetOff(void)
{
  HAL_GPIO_WritePin(AD_RESET_PORT, AD_RESET_PIN, GPIO_PIN_RESET);
}

void gpioCLKOn(void)
{
  HAL_GPIO_WritePin(CLK_PORT, CLK_PIN, GPIO_PIN_SET);
}

void gpioCLKOff(void)
{
  HAL_GPIO_WritePin(CLK_PORT, CLK_PIN, GPIO_PIN_RESET);
}

void gpioMOSI(uint8_t data)
{
  if (data) HAL_GPIO_WritePin(MOSI_PORT, MOSI_PIN, GPIO_PIN_SET);
  else HAL_GPIO_WritePin(MOSI_PORT, MOSI_PIN, GPIO_PIN_RESET);
}

void gpioADCSOn(void)
{
  HAL_GPIO_WritePin(AD_CS_PORT, AD_CS_PIN, GPIO_PIN_SET);
}

void gpioADCSOff(void)
{
  HAL_GPIO_WritePin(AD_CS_PORT, AD_CS_PIN, GPIO_PIN_RESET);
}

/* Private functions ---------------------------------------------------------*/

/**
  * @brief  
  * @param  None
  * @param  None
  * @retval None
  */


/**
  * @}
  */ 
